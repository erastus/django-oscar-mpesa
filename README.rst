**Django-oscar M-Pesa**

This package provides integration between django-oscar and M-Pesa's Instant
Payment Notification (IPN) service.

**Sandbox**

You can get a feel of how this module works by running the sandbox site:
::

    mkvirtualenv django-oscar-mpesa
    make sandbox
    sandbox/manage.py runserver



**Documentation**

The official documentation can be found at http://django-oscar-mpesa.readthedocs.org.

**Running tests**

::

    ./runtests.py
