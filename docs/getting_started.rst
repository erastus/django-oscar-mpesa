===============
Getting Started
===============

Safaricom's Instant Payment Notification (IPN) service allows organisations to
receive rapid notification of M-Pesa payments made to their Pay Bill number.

When signing up for IPN, you will be asked to provide an endpoint URL to Safaricom. You will need to create an endpoint in your project's urls.py file. The view used for this url is `mpesa.views.IPNReceiverView`. An example would look like this::

    urlpatterns += pattern('',
    	url(r"^ipn/?$", mpesa.views.IPNReceiverView.as_view(), name="ipn-receiver"),
    )

Now IPN notifications can be received at `/ipn`. This is the URL you will supply to Safaricom.

The IPN service provides basic password authentication for notifications. You 
will be asked to supply a username and password when applying for IPN. These
parameters will be sent with every IPN notification.

Add your chosen username and password to your settings file as follows::

    MPESA_IPN_USER = 'username'
    MPESA_IPN_PASS = 'password'

.. WARNING:: If the MPESA_IPN_USER and MPESA_IPN_PASS settings do not match the username and password used in your IPNs, the IPNs will be ignored.

Add your Pay Bill number to the settings::

    MPESA_PAYBILL_NUMBER = '123456'

This will be used in the payment instructions displayed to buyers.
	
If you're using oscar's dashboard app, extend the dashboard navigation::

	from oscar.defaults import *
	from django.utils.translation import ugettext_lazy as _
	OSCAR_DASHBOARD_NAVIGATION.append(
		{
			'label': _('M-Pesa'),
			'icon': 'icon-globe',
			'children': [
				{
					'label': _('M-Pesa transactions'),
					'url_name': 'mpesa-payments-list',
				},
			]
		})


