============
How it works
============

Safaricom's Instant Payment Notification (IPN) service allows organisations to
receive real-time notification of M-Pesa payments made to their Pay Bill number.

An IPN is a notification that is sent every time a payment is made to your to your Pay Bill account. Safaricom sends this notification in the form of a HTTP GET request.

This module provides integration between the IPN service and Oscar, making it
it easy to receive and allocate M-Pesa payments for online purchases on your 
Oscar site. The process is, broadly, as follows:

1. A 'Pay by M-Pesa' option is made available to the customer at checkout.
2. The customer is instructed to send the order amount to the Pay Bill number, and enter the transaction reference provided by Safaricom.
3. When an IPN notification matching that transaction reference is received, the payment is allocated to the order.