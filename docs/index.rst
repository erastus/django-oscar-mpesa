.. django-oscar-mpesa documentation master file, created by
   sphinx-quickstart on Fri Oct 17 16:39:04 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-oscar-mpesa's documentation!
===============================================

This package provides integration between django-oscar and M-Pesa's Instant
Payment Notification (IPN) service.

Installation
------------

Install the package using either::

    pip install django-oscar-mpesa

or::

    pip install https://bitbucket.org/regulusweb/django-oscar-mpesa/get/master.zip

Add ``mpesa`` to your ``INSTALLED_APPS``, and add mpesa's url configuration to your urlpatterns::

    urlpatterns = patterns('',
        # your other url patterns go here

        (r'', include(mpesa.urls))
    )

Finally run::

    python manage.py syncdb

Contents:

.. toctree::
   :maxdepth: 2

   how_it_works
   getting_started
   order_handling
   test_tool
   limitations



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

