==============================
Limitations of the IPN service
==============================

The IPN service has some limitations which developers should be aware of. All of these are undocumented "features" and have only been learnt of through experience.

* Responses are not sent back to the customer.
	The IPN documentation provided by Safaricom suggests that you can respond to a payment with either a "success" or "failure" message, which should determine whether they payment is accepted, and send the message to th cutomer. For example a response like this::

		"OK|Custom Message"

	... would send an SMS to the customer with the message ``Custom Message``. This could be useful, for example, for letting a customer that they have underpaid/overpaid for an order.

	Unfortunately, this feature hasn't been implemented by Safaricom. IPN is at present a notification-only service and the response has no effect on the status of the payment. It isn't relayed to the customer, either.

* Failed notifications are not re-sent.
	If a notification is sent while your server is experiencing down-time, Safaricom will not make an attempt to send it again in the future.

* Notifications **might** be sent more than once for the same payment.
	These duplicate IPNs are ignored.

* Passwords longer than 20 characters are truncated.
	Keep your authentication password shorter than 20 characters.

* An IPN is sent when the Paybill account holder withdraws money from their account.
	From the perspective of this module, this is not a payment and will therefore be ignored.