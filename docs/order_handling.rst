==============
Order Handling
==============

This module works in conjunction with oscar's ``payment`` app to track payments. Note that this module does not do any order processing for you.

When an order is created a ``Source`` of type `M-Pesa` is created for the order. Every time a payment is made to your Pay Bill account, a ``Transaction`` indicating the amount paid, will be added to this Source and a `signal 
<https://docs.djangoproject.com/en/dev/topics/signals/>`_ is sent containing the M-Pesa payment and the transaction.

Your application should listen for this signal, and use it to check if the order has been fully paid and take the appropriate steps thereafter.

The sandbox contains a sample implementation of order processing. See `sandbox/apps/order/processing.py <https://bitbucket.org/regulusweb/django-oscar-mpesa/src/master/sandbox/apps/order/processing.py>`_.
