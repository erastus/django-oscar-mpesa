==================
Fake IPN Generator
==================

For testing purposes, there is an IPN generator page. This can be used to test your order handling code.

By default the IPN generator can be accessed at `/en-gb/ipn-gen`.