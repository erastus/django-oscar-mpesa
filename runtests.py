#!/usr/bin/env python
import sys
from optparse import OptionParser
import logging

import django
from django.conf import settings

logging.disable(logging.CRITICAL)

if not settings.configured:
    mpesa_settings = {}
    try:
        from integration import *
    except ImportError:
        mpesa_settings.update({
            'MPESA_IPN_USER': 'test_user',
            'MPESA_IPN_PASS': 'test_pass',
            'MPESA_PAYBILL_NUMBER': '920012',
            'PHONENUMBER_DEFAULT_REGION': 'KE'
        })
    else:
        for key, value in list(locals().items()):
            if key.startswith('MPESA'):
                mpesa_settings[key] = value

    from oscar.defaults import *
    for key, value in list(locals().items()):
        if key.startswith('OSCAR'):
            mpesa_settings[key] = value
    mpesa_settings['OSCAR_EAGER_ALERTS'] = False

    from oscar import get_core_apps

    sandbox_settings = {
        'DATABASES': {
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
            }
        },
        'INSTALLED_APPS': [
            'django.contrib.auth',
            'django.contrib.admin',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.sites',
            'mpesa',
        ] + get_core_apps(),
        'DEBUG': False,
        'SITE_ID': 1,
        'ROOT_URLCONF': 'tests.urls',
        'NOSE_ARGS': ['-s'],
        'HAYSTACK_CONNECTIONS': {
            'default': {
                'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
            },
        },
        'MIDDLEWARE_CLASSES': (
                'django.middleware.common.CommonMiddleware',
                'django.contrib.sessions.middleware.SessionMiddleware',
                'django.middleware.csrf.CsrfViewMiddleware',
                'django.contrib.auth.middleware.AuthenticationMiddleware',
                'django.contrib.messages.middleware.MessageMiddleware',
                'oscar.apps.basket.middleware.BasketMiddleware',
        ),
    }
    sandbox_settings.update(mpesa_settings)
    settings.configure(**sandbox_settings)

# Needs to be here to avoid missing SETTINGS env var
from django_nose import NoseTestSuiteRunner


def run_tests(*test_args):
    if not test_args:
        test_args = ['tests']

    # Run tests
    test_runner = NoseTestSuiteRunner(verbosity=1)

    num_failures = test_runner.run_tests(test_args)

    if num_failures > 0:
        sys.exit(num_failures)

if __name__ == '__main__':
    parser = OptionParser()
    (options, args) = parser.parse_args()
    run_tests(*args)
