from django.views.generic import TemplateView

from oscar.apps.checkout.views import PaymentMethodView as BasePaymentMethodView


class PaymentMethodView(BasePaymentMethodView):

    template_name = "checkout/payment-methods.html"

    def get(self, request, **kwargs):
        return TemplateView.get(self, request, **kwargs)