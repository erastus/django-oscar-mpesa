from django.dispatch import receiver
from django.db.models import get_model
from django.conf import settings

from oscar.apps.order.processing import EventHandler as BaseEventHandler

from mpesa.signals import payment_accepted

PaymentEventType = get_model("order", "PaymentEventType")


@receiver(payment_accepted)
def mpesa_payment_signal_handler(sender, **kwargs):
    """
        In this sandbox app, orders are fulfilled automatically when payment is received
    """

    payment = kwargs.get("mpesa_payment")
    transaction = kwargs.get("transaction")

    order = transaction.source.order
    order_lines = sorted(list(order.lines.all()))
    order_line_quantities = [line.quantity for line in order_lines]

    source = transaction.source

    event_type, _ = PaymentEventType.objects.get_or_create(name="PAID")

    event_handler = EventHandler()
    # The order might have multiple sources source we have to pass
    # the one we are interested in
    event_handler.handle_payment_event(order, event_type,
                transaction.amount, order_lines, order_line_quantities,
                source=source)


class EventHandler(BaseEventHandler):

    def handle_payment_event(self, order, event_type, amount, lines=None,
                             line_quantities=None, **kwargs):

        event = super(EventHandler, self).handle_payment_event(order, event_type, amount, lines,
                             line_quantities, **kwargs)

        order_status = settings.ORDER_PARTIALLY_PAID

        source = kwargs.get("source")

        if source.amount_debited >= source.amount_allocated:
            order_status = settings.ORDER_FULLY_PAID

        order.set_status(order_status)