from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

from apps.app import application

from mpesa.views import IPNReceiverView
from mpesa.dashboard.app import application as mpesa_dashboard_app
from mpesa.urls import urlpatterns as mpesa_urls

admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^ipn/?$', IPNReceiverView.as_view(), name="ipn-receiver"),
)
urlpatterns += i18n_patterns('',
    (r'', include(application.urls)),
    (r'', include(mpesa_urls))
)
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

