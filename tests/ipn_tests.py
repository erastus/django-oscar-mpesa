from django.test.client import RequestFactory
from django.test import TestCase
from django.conf import settings

from mpesa.views import IPNReceiverView
from mpesa.models import MpesaPayment

dummy_transaction_data = {
    'id': '12345678',
    'orig': 'MPESA',
    'dest': '254722456789',
    'tstamp': '2014-06-30 11:18:00',
    'text': 'ABCD1234 Confirmed. on 30/6/14 at 11:17 AM Ksh10.00 '
            'received from A Man 254722123456. Account Number ETEIKRLWR '
            'New Utility balance is Ksh30.00',
    'customer_id': '1234',
    'user': 'small',
    'pass': 'goat',
    'routemethod_id': '2',
    'routemethod_name': 'HTTP',
    'mpesa_code': 'ABCD1234',
    'mpesa_acc': 'ETEIKRLWR',
    'mpesa_msisdn': '254722123456',
    'mpesa_trx_date': '30/6/14',
    'mpesa_trx_time': '11:17 AM',
    'mpesa_amt': '10.0',
    'mpesa_sender': 'A Man',
    'business_number': '100000',
}


class TestIPNConfirmation(TestCase):

    missing_param_list = ['mpesa_code', 'mpesa_amt']

    view = IPNReceiverView

    def setUp(self):
        self.rf = RequestFactory()
        self.ipn_endpoint = '/some/ipn/handler/'
        settings.MPESA_IPN_USER = "small"
        settings.MPESA_IPN_PASS = "goat"

    def assert_no_payment_was_created(self):
        self.assertEqual(MpesaPayment.objects.count(), 0)

    def test_bad_auth(self):
        bad_auth_params = dummy_transaction_data.copy()
        bad_auth_params.update({
            "user": "big",
            "pass": "dog"
        })
        req = self.rf.get(self.ipn_endpoint, bad_auth_params)
        resp = self.view.as_view()(req)
        # Response should be 401
        self.assertEqual(resp.status_code, 401)
        # Content of response is prescribed
        self.assertEqual(resp.content, IPNReceiverView.AUTHENTICATION_FAILURE_MSG)

        self.assert_no_payment_was_created()

    def test_withdraw_ipn_is_dropped(self):
        withdraw_ipn_params = dummy_transaction_data.copy()
        withdraw_ipn_params.update({
            "mpesa_amt": "-1"
        })

        request = self.rf.get(self.ipn_endpoint, withdraw_ipn_params)
        response = self.view.as_view()(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.content, IPNReceiverView.PAYBILL_ERROR)

        self.assert_no_payment_was_created()

    def test_missing_data(self):
        missing_data_params = dummy_transaction_data.copy()
        for item in self.missing_param_list:
            missing_data_params.pop(item)

        # Trigger a fake IPN notification
        req = self.rf.get(self.ipn_endpoint, missing_data_params)
        resp = self.view.as_view()(req)
        # Response should be 422
        self.assertEqual(resp.status_code, 200)
        # Content of response is prescribed
        self.assertEqual(resp.content, IPNReceiverView.PAYBILL_ERROR)

        self.assert_no_payment_was_created()

    def test_transaction_sent_multiple_times_fails(self):
        req = self.rf.get(self.ipn_endpoint, dummy_transaction_data)
        resp1 = self.view.as_view()(req)

        resp2 = self.view.as_view()(req)
        # Response should be 400
        self.assertEqual(resp2.status_code, 200)
        # Content of response is prescribed
        self.assertEqual(resp2.content, IPNReceiverView.PAYBILL_ERROR)

    def test_accepted(self):
        # Trigger a fake IPN notification
        req = self.rf.get(self.ipn_endpoint, dummy_transaction_data)
        resp = self.view.as_view()(req)
        # Response should be 200
        self.assertEqual(resp.status_code, 200)
        # Content of response is prescribed
        self.assertEqual(resp.content, IPNReceiverView.PAYMENT_ACCEPTED_MSG)
