import datetime
from decimal import Decimal as D

from django.test import TestCase

from phonenumber_field.modelfields import PhoneNumber

from mpesa.forms import IPNReceiverForm
from mpesa.utils import parse_ipn_data

dummy_transaction_data = {
    'id': '12345678',
    'orig': 'MPESA',
    'dest': '254722456789',
    'tstamp': '2014-06-30 11:18:00',
    'text': 'ABCD1234 Confirmed. on 30/6/14 at 11:17 AM Ksh10.00 '
            'received from A Man 254722123456. Account Number ETEIKRLWR '
            'New Utility balance is Ksh30.00',
    'customer_id': '1234',
    'user': 'small',
    'pass': 'goat',
    'routemethod_id': '2',
    'routemethod_name': 'HTTP',
    'mpesa_code': 'ABCD1234',
    'mpesa_acc': 'ETEIKRLWR',
    'mpesa_msisdn': '254722123456',
    'mpesa_trx_date': '30/6/14',
    'mpesa_trx_time': '11:17 AM',
    'mpesa_amt': '10.0',
    'mpesa_sender': 'A Man',
    'business_number': '100000',
}


class MpesaTransactionFormTest(TestCase):

    def get_form(self, data):
        return IPNReceiverForm(parse_ipn_data(data))

    def get_transaction(self, data):
        form = self.get_form(data)
        if form.is_valid():
            return form.save()

        raise Exception(form.errors)

    def test_transaction_creation_from_data(self):

        transaction = self.get_transaction(dummy_transaction_data)

        self.assertEqual(transaction.ipn_id, "12345678")
        self.assertEqual(transaction.amount, D(10))
        self.assertEqual(transaction.origin, "MPESA")
        self.assertEqual(transaction.recieved_at, datetime.datetime(2014, 6, 30, 11, 18))
        self.assertEqual(transaction.customer_id, "1234")
        self.assertEqual(transaction.reference_number, "ABCD1234")
        self.assertEqual(transaction.paybill_number, 100000)
        self.assertEqual(transaction.account, "ETEIKRLWR")

        self.assertEqual(transaction.message, "ABCD1234 Confirmed. on 30/6/14 at "
                                              "11:17 AM Ksh10.00 received from A Man "
                                              "254722123456. Account Number ETEIKRLWR "
                                              "New Utility balance is Ksh30.00")
        self.assertEqual(transaction.destination, PhoneNumber.from_string("254722456789"))
        self.assertEqual(transaction.subscriber_phone_number, PhoneNumber.from_string("254722123456"))
        self.assertEqual(transaction.subscriber_name, "A Man")

        self.assertEqual(transaction.transaction_datetime, datetime.datetime(2014, 6, 30, 11, 17))

    def test_paybill_account_can_be_excluded(self):

        transaction_form = self.get_form(dummy_transaction_data)
        self.assertTrue(transaction_form.is_valid())

        data_without_account = dummy_transaction_data.copy()
        data_without_account.pop("mpesa_acc")
        # changed to avoid this being flagged as duplicate
        data_without_account["mpesa_code"] = "1234"
        data_without_account["id"] = "123322"

        transaction_form2 = self.get_form(data_without_account)
        self.assertTrue(transaction_form2.is_valid())

    def test_missing_data_is_invalid(self):
        missing_data_params = dummy_transaction_data.copy()
        for item in ["mpesa_code"]:
            missing_data_params.pop(item)

        transaction_form = self.get_form(missing_data_params)
        self.assertFalse(transaction_form.is_valid())

    def test_duplicated_transaction_is_invalid(self):
        self.get_transaction(dummy_transaction_data)

        duplicate_transaction_form = self.get_form(dummy_transaction_data)

        self.assertFalse(duplicate_transaction_form.is_valid())
