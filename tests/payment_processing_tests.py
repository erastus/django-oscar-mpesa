from django.test import TestCase

from mpesa.forms import IPNReceiverForm
from mpesa.utils import parse_ipn_data
from mpesa.signals import ipn_received
from mpesa.models import PAYMENT_RECEIVED, PAYMENT_PENDING

from oscar.apps.payment.models import Transaction, SourceType, Source
from oscar.test.factories import create_order


class PaymentProcessingTests(TestCase):

    def create_payment(self, amount=50, ref_no="ABCD1234"):
        mpesa_payment = IPNReceiverForm(parse_ipn_data({
            'id': '12345678',
            'orig': 'MPESA',
            'dest': '254722456789',
            'tstamp': '2014-06-30 11:18:00',
            'text': 'ABCD1234 Confirmed. on 30/6/14 at 11:17 AM Ksh10.00 '
                    'received from A Man 254722123456. Account Number ETEIKRLWR '
                    'New Utility balance is Ksh30.00',
            'customer_id': '1234',
            'user': 'small',
            'pass': 'goat',
            'routemethod_id': '2',
            'routemethod_name': 'HTTP',
            'mpesa_code': ref_no,
            'mpesa_acc': 'ETEIKRLWR',
            'mpesa_msisdn': '254722123456',
            'mpesa_trx_date': '30/6/14',
            'mpesa_trx_time': '11:17 AM',
            'mpesa_amt': amount,
            'mpesa_sender': 'A Man',
            'business_number': '100000'
        })).save()
        return mpesa_payment


    def test_process_payment_notification(self):
        """
            creates order, adds payment source, creates mpesa payment

            tests whether the payment received from IPN is matched against the payment source
            that was created.
        """

        ref_no = "REFFFFFFF"

        order = create_order()

        source_type, __ = SourceType.objects.get_or_create(name='M-Pesa')
        source = order.sources.create(source_type=source_type,
                        amount_allocated=order.total_incl_tax,
                        currency="KES")

        source.create_deferred_transaction(Transaction.DEBIT, 0,
                        reference=ref_no, status=PAYMENT_PENDING)
        source.save()

        self.assertEqual(source.transactions.all()[0].status, PAYMENT_PENDING)
        self.assertEqual(source.transactions.all()[0].amount, 0)
        self.assertEqual(source.amount_allocated, order.total_incl_tax)
        self.assertEqual(source.amount_debited, 0)

        mpesa_payment = self.create_payment(amount=order.total_incl_tax, ref_no=ref_no)

        ipn_received.send(self, mpesa_payment=mpesa_payment)

        # ORM not updated after saving, need to fetch object again
        source = Source.objects.get(pk=source.pk)

        self.assertEqual(source.transactions.all()[0].status, PAYMENT_RECEIVED)
        self.assertEqual(source.transactions.all()[0].amount, order.total_incl_tax)
        self.assertEqual(source.amount_debited, order.total_incl_tax)
